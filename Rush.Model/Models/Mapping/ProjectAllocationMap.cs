﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rush.Model.Models.Mapping
{
    public class ProjectAllocationMap : EntityTypeConfiguration<ProjectAllocation>
    {
        public ProjectAllocationMap()
        {
            HasKey(t => t.AllocationId);

            Property(t => t.AllocatedBy).IsRequired();
            Property(t => t.AllocatedDate).IsRequired();
            Property(t => t.DeveloperId);
            Property(t => t.ProjectId);
            Property(t => t.FromDate).IsRequired();
            Property(t => t.ToDate).IsRequired();
            Property(t => t.WorkingCategoryId).IsRequired();
            Property(t => t.BillingTypeId).IsRequired();


            //Table and Column mappng
            ToTable("ProjectAllocations");
            Property(t => t.DeveloperId).HasColumnName("DeveloperId");
            Property(t => t.ProjectId).HasColumnName("ProjectId");
            Property(t => t.AllocatedBy).HasColumnName("AllocatedBy");
            Property(t => t.AllocatedDate).HasColumnName("AllocatedDate");

            Property(t => t.FromDate).HasColumnName("FromDate");
            Property(t => t.ToDate).HasColumnName("ToDate");
            Property(t => t.WorkingCategoryId).HasColumnName("WorkingCategoryId");
            Property(t => t.BillingTypeId).HasColumnName("BillingTypeId");

            // Composite foreign key 
            HasRequired(c => c.Developer)
                .WithMany()
                .HasForeignKey(d => new {d.DeveloperId});

            HasRequired(c => c.Project)
               .WithMany()
               .HasForeignKey(d => new { d.ProjectId });

            HasRequired(c => c.BillingType)
                .WithMany()
                .HasForeignKey(d => new { d.BillingTypeId });
        }
    }
}

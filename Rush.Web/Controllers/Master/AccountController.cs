﻿using Rush.Web.ViewModels;

namespace Rush.Web.Controllers.Master
{
    public class AccountController : BaseApiController
    {
       // GET api/account/5
        public AccountViewModel Get(string userId, string passWord)
        {
            return new AccountViewModel { IsAuthenticated = true, UserId = userId };
        }

    }
}

﻿var developerAsssignmentUrl = 'api/DeveloperAssignment';

rushApp.factory('developerAssignmentRepository', function ($http) {
    return {
        getDeveloperAssignment: function (callback, developerId) {
            $http.get(developerAsssignmentUrl + '/' + developerId).success(callback);
        },
        insertDeveloperAssignment: function (callback, developerAssignment) {
            $http.post(developerAsssignmentUrl, developerAssignment).success(callback);
        },
        updateDeveloperAssignment: function (callback, developerAssignment) {
            $http.put(developerAsssignmentUrl + '/' + developerAssignment.AllocationId, developerAssignment).success(callback);
        },
        deleteDeveloperAssignment: function (callback, id) {
            $http.delete(developerAsssignmentUrl + '/' + id).success(callback);
        }
    };
});

rushApp.controller('developerAssignmentController', function ($scope, developerAssignmentRepository, $routeParams) {

    $scope.developerId = $routeParams.id;
    getDeveloperAssignments();

    function getDeveloperAssignments() {
        developerAssignmentRepository.getDeveloperAssignment(function (results) {
            $scope.developer = results.Developer;
            $scope.developerData = results.DeveloperAssignments;
            $scope.projectData = results.Projects;
            $scope.billingData = results.BillingTypes;
            $scope.workCategoryData = results.WorkCategories;
        }, $scope.developerId);
    }

    $scope.setScope = function (obj, action) {
        $scope.action = action;
        $scope.New = obj;
    };

    $scope.setNew = function () {
        var newObj = { 'DeveloperId': 0, 'ProjectId': 0, 'FromDate': '', 'ToDate': '', 'WorkingCategoryId': 0, 'BillingTypeId': 0 };
        $scope.New = setKey(newObj);
    };

    function setKey() {
        $scope.New.DeveloperId = $scope.developerId;
    }



    $scope.gridOptions = {
        data: 'developerData',
        showGroupPanel: false,
        columnDefs: [{ field: 'AllocationId', displayName: 'AllocationId', visible: false, width: '10%' },
            { field: 'DeveloperId', displayName: 'Developer Id', visible: false, width: '10%' },
            { field: 'ProjectId', displayName: 'Project Id', visible: false, width: '10%' },
            { field: 'ProjectName', displayName: 'Project Name', width: '15%' },
            { field: 'FromDate', displayName: 'From Date', width: '15%' },
            { field: 'ToDate', displayName: 'To Date', width: '15%' },
            { field: 'AllocatedDate', displayName: 'Allocated Date', width: '15%' },
            { field: 'WorkingCategoryId', displayName: 'Working Category', visible: false, width: '15%' },
            { field: 'WorkingCategory', displayName: 'Working Category', width: '15%' },
            { field: 'BillingTypeId', displayName: 'BillingType Id', visible: false, width: '30%' },
            { field: 'BillingType', displayName: 'BillingType', width: '15%' },
            {
                displayName: 'Options', cellTemplate: '<input type="button" ng-click="setScope(row.entity,\'edit\')"  name="assign"  value="Edit">' +
                   '&nbsp;<input type="button" ng-click="delete(row.entity.AllocationId)"  name="delete"  value="Delete">', width: '10%'
            }

        ]
    };

    $scope.update = function () {
        if ($scope.action == 'edit') {
            developerAssignmentRepository.updateDeveloperAssignment(function () {
                var status = 'Assignment updated successfully';
                $scope.status = status;
                alert(status);
                getDeveloperAssignments();
            }, $scope.New);
        } else {
            setKey();
            developerAssignmentRepository.insertDeveloperAssignment(function () {
                alert('Assignment inserted successfully');
                getDeveloperAssignments();
            }, $scope.New);
        }
    };
    
    $scope.delete = function (id) {
        var retVal = confirm("Are you sure want to remove?");
        if (retVal == true) {
            developerAssignmentRepository.deleteDeveloperAssignment(function () {
                alert('Assignment deleted successfully');
                getDeveloperAssignments();
            }, id);
        }
    };
    
    $scope.today = function () {
        $scope.dt = new Date();
    };
    $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function (date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.initDate = new Date('2016-15-20');
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];


});
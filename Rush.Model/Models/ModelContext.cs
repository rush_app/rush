﻿
using System.Data.Entity;
using Rush.Model.Models.Mapping;

namespace Rush.Model.Models
{
    public class ModelContext : DbContext
    {
        public ModelContext(string connectionString)
            : base(connectionString)
        {

        }

        public ModelContext()
            : this("Name=DbConnection")
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;

        }

        public DbSet<Developer> Developpers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectAllocation> ProjectAllocations { get; set; }
        public DbSet<BillingType> BillingTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DeveloperMap());
            modelBuilder.Configurations.Add(new ProjectMap());
            modelBuilder.Configurations.Add(new ProjectAllocationMap());
            modelBuilder.Configurations.Add(new BillingTypeMap());
        }
    }
}

﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Rush.Model.Models;
using Rush.Web.ViewModels;

namespace Rush.Web.Controllers.AssignDevelopers
{
    public class AssignDeveloperController : BaseApiController
    {
        
        // GET api/assigndeveloper/5
        public AssignDeveloperGetViewModel Get(int id)
        {
            var projectInfo = DbContext.Projects.Find(id);
            var allocatedDevelopers = DbContext.ProjectAllocations.Where(c => c.ProjectId == id).ToList();
            var developers = DbContext.Developpers.ToList();
            var assignedDevelopers =
                developers.Select(
                    developer =>
                        new AssignDeveloperViewModel
                        {
                            ProjectId = id,
                            DeveloperId = developer.DeveloperId,
                            DeveloperName = developer.DeveloperName,
                            IsAssigned = allocatedDevelopers.Any(c => c.DeveloperId == developer.DeveloperId)
                        }).ToList();

            var viewModel = new AssignDeveloperGetViewModel
            {
                Developers = assignedDevelopers,
                Projects = projectInfo
            };
            return viewModel;
        }

        // POST api/assigndeveloper
        public void Post([FromBody]AssignDeveloperViewModel assignDeveloper)
        {
            var assigendDeveloper = new ProjectAllocation();
            if (assignDeveloper.IsAssigned)
            {
                assigendDeveloper.DeveloperId = assignDeveloper.DeveloperId;
                assigendDeveloper.ProjectId = assignDeveloper.ProjectId;
                assigendDeveloper.AllocatedDate = DateTime.Now;
                assigendDeveloper.AllocatedBy = 1; //Not desided yet
                DbContext.Entry(assigendDeveloper).State = EntityState.Added;
            }
            else
            {
                assigendDeveloper =
                    DbContext.ProjectAllocations.Single(
                        c => c.ProjectId == assignDeveloper.ProjectId && c.DeveloperId == assignDeveloper.DeveloperId);
                DbContext.ProjectAllocations.Remove(assigendDeveloper);
            }

            DbContext.SaveChanges();
        }
      
    }
}

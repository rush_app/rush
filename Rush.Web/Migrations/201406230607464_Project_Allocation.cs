namespace Rush.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Project_Allocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProjectAllocations", "FromDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ProjectAllocations", "ToDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ProjectAllocations", "WorkingCategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.ProjectAllocations", "BillingTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.ProjectAllocations", "BillingTypeId");
            AddForeignKey("dbo.ProjectAllocations", "BillingTypeId", "dbo.BillingTypes", "BillingTypeId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectAllocations", "BillingTypeId", "dbo.BillingTypes");
            DropIndex("dbo.ProjectAllocations", new[] { "BillingTypeId" });
            DropColumn("dbo.ProjectAllocations", "BillingTypeId");
            DropColumn("dbo.ProjectAllocations", "WorkingCategoryId");
            DropColumn("dbo.ProjectAllocations", "ToDate");
            DropColumn("dbo.ProjectAllocations", "FromDate");
        }
    }
}

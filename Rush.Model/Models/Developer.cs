﻿
using System.ComponentModel.DataAnnotations;

namespace Rush.Model.Models
{
    public class Developer : Person
    {
        public int DeveloperId { get; set; }
        [Required(ErrorMessage = "Developer name required")]
        public string DeveloperName { get; set; }
    }
}

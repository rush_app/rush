﻿'use strict';

var loginApp = angular.module('loginApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize'
]);


//app.config(function ($routeProvider, $locationProvider, $httpProvider) {
//    $httpProvider.responseInterceptors.push('httpInterceptor');

//    $routeProvider
//        .when('/', { templateUrl: 'Views/Home.html', controller: 'home' })
//        .when('/login', { templateUrl: 'views/auth.html', controller: 'auth' })
//        .otherwise({ redirectTo: '/' });

//    $locationProvider.html5Mode(true);
//});

//app.run(function (api) {
//    api.init();
//})

var url = 'api/Login';

loginApp.factory('loginRepository', function($http) {
    return {        
        getAuthentication: function(callback) {
            $http.get(url).success(callback);
        }
    };
});

loginApp.controller('loginCtrl', function($scope, loginRepository) {

    $scope.login = function() {
        loginRepository.getAuthentication(function() {
            $scope.status = true;
        });
    };

});

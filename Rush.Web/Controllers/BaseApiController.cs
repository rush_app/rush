﻿using System.Collections;
using System.Collections.Generic;
using System.Web.Http;
using Rush.Model.Models;
using Rush.Web.Enums;
using Rush.Web.Filters;
using Rush.Web.ViewModels;

namespace Rush.Web.Controllers
{
    [UserAuthenticationAttribute]
    public class BaseApiController : ApiController
    {
        public ModelContext DbContext;

        public BaseApiController()
        {
            DbContext = new ModelContext();
        }

        protected IEnumerable<IdAndText> GetCategoryList()
        {
            var workCategories = new List<IdAndText>
            {
                new IdAndText {Id = (int) WorkCategory.FullTime, Text = WorkCategory.FullTime.ToString()},
                new IdAndText {Id = (int) WorkCategory.PartTime, Text = WorkCategory.PartTime.ToString()},
                new IdAndText {Id = (int) WorkCategory.TimeAndMaterial, Text = WorkCategory.TimeAndMaterial.ToString()}
            };
            return workCategories;
        }
    }
}
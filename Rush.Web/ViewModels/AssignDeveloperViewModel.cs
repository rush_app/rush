﻿
namespace Rush.Web.ViewModels
{
    public class AssignDeveloperViewModel
    {
        public int ProjectId { get; set; }
        public int DeveloperId { get; set; }
        public string DeveloperName { get; set; }
        public bool IsAssigned { get; set; }
    }
}
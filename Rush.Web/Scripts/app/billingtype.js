﻿var billingTypeUrl = 'api/BillingType';


rushApp.factory('billingTypeRepository', function ($http) {
    return {
        getBillingTypes: function (callback) {
            $http.get(billingTypeUrl).success(callback);
        },
        insertBillingType: function (callback, billingtype) {
            $http.post(billingTypeUrl, billingtype).success(callback);
        },
        updateBillingType: function (callback, billingtype) {
            $http.put(billingTypeUrl + '/' + billingtype.BillingTypeId, billingtype).success(callback);
        },
        deleteBillingType: function (callback, id) {
            $http.delete(billingTypeUrl + '/' + id).success(callback);
        }
    };
});

//Controller
rushApp.controller('billingTypeController', function ($scope, billingTypeRepository) {
    getBillingTypes();

    function getBillingTypes() {

        billingTypeRepository.getBillingTypes(function (results) {
            $scope.billingData = results;
        });
    }

    $scope.setScope = function (obj, action) {
        $scope.action = action;
        $scope.New = obj;
    };

    $scope.gridOptions = {
        data: 'billingData',
        showGroupPanel: true,
        columnDefs: [{ field: 'BillingTypeId', displayName: 'Billing Type Id', width: '10%' },
            { field: 'Description', displayName: 'Description', width: '30%' },
            { field: 'BillablePercentage', displayName: 'Billable Percentage', width: '10%' },
            { displayName: 'Options', cellTemplate: '<input type="button" ng-click="setScope(row.entity,\'edit\')" name="edit"  value="Edit">&nbsp;<input type="button" ng-click="delete(row.entity.BillingTypeId)"  name="delete"  value="Delete">', width: '15%' }
        ]
    };

    $scope.update = function () {
        if ($scope.action == 'edit') {
            billingTypeRepository.updateBillingType(function () {
                var status = 'Billing type updated successfully';
                $scope.status = status;
                alert(status);
                getBillingTypes();
            }, $scope.New);
        } else {
            billingTypeRepository.insertBillingType(function () {
                alert('Billing type inserted successfully');
                getBillingTypes();
            }, $scope.New);
        }
    };

    $scope.delete = function (id) {
        var retVal = confirm("Are you sure want to remove?");
        if (retVal == true) {
            billingTypeRepository.deleteBillingType(function () {
                alert('Billing type deleted successfully');
                getBillingTypes();
            }, id);
        }
    };
});
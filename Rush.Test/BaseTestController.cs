﻿
using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Rush.Test
{
    public class BaseTestController
    {
        private HttpServer _server;
        private string _url = "http://localhost/";
        readonly HttpConfiguration config = new HttpConfiguration();

        public BaseTestController()
        {
            config.Routes.MapHttpRoute(name: "Default", routeTemplate: "api/{controller}/{action}/{id}", defaults: new { id = RouteParameter.Optional });
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            _server = new HttpServer(config);
        }

        public void SetRequetTestObject<TObj>(TObj testObject, out HttpClient client, out HttpRequestMessage request, string apiPath) where TObj : class
        {
           // _server = new HttpServer(config);
            client = new HttpClient(_server);
            request = CreateRequest(apiPath, "application/json", HttpMethod.Post, testObject, new JsonMediaTypeFormatter());
        }

        public void SetRequetTestObject(out HttpClient client, out HttpRequestMessage request, string apiPath)
        {
           // _server = new HttpServer(config);
            client = new HttpClient(_server);
            request = CreateRequest(apiPath, "application/json", HttpMethod.Get);
        }

        private HttpRequestMessage CreateRequest<T>(string url, string mthv, HttpMethod method, T content, MediaTypeFormatter formatter) where T : class
        {
            HttpRequestMessage request = CreateRequest(url, mthv, method);
            request.Content = new ObjectContent<T>(content, formatter);
            return request;
        }
        private HttpRequestMessage CreateRequest(string url, string mthv, HttpMethod method)
        {
            var request = new HttpRequestMessage { RequestUri = new Uri(_url + url) };
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mthv));
            request.Method = method;
            return request;
        }
    }
}

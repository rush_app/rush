namespace Rush.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Billing_Types : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillingTypes",
                c => new
                    {
                        BillingTypeId = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 50),
                        BillablePercentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.BillingTypeId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BillingTypes");
        }
    }
}

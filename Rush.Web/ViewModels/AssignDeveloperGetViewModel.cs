﻿
using System.Collections.Generic;
using Rush.Model.Models;

namespace Rush.Web.ViewModels
{
    public class AssignDeveloperGetViewModel
    {
        public Project Projects { get; set; }
        public IEnumerable<AssignDeveloperViewModel> Developers { get; set; }
    }
}
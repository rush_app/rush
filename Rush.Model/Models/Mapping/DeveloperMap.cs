﻿
using System.Data.Entity.ModelConfiguration;

namespace Rush.Model.Models.Mapping
{
    public class DeveloperMap : EntityTypeConfiguration<Developer>
    {
        public DeveloperMap()
        {
            //PrimaryKey
            HasKey(t => t.DeveloperId);

            //Properties
            Property(t => t.DeveloperName).IsRequired().HasMaxLength(100);
            Property(t => t.Tel).HasMaxLength(30);
            Property(t => t.Email).HasMaxLength(30);
            Property(t => t.IsActive).IsRequired();

            //Table and Column mappng
            ToTable("Developers");
            Property(t => t.DeveloperId).HasColumnName("DeveloperId");
            Property(t => t.DeveloperName).HasColumnName("DeveloperName");
            Property(t => t.IsActive).HasColumnName("IsActive");
            Property(t => t.Email).HasColumnName("Email");
        }
    }
}

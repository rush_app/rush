﻿
using System;

namespace Rush.Model.Models
{
    public class ProjectAllocation
    {
        public int AllocationId { get; set; }
        public int ProjectId { get; set; }
        public int DeveloperId { get; set; }
        public int AllocatedBy { get; set; }
        public DateTime AllocatedDate { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public int WorkingCategoryId { get; set; }
        public int BillingTypeId { get; set; }

        public virtual BillingType BillingType { get; set; }
        public virtual Developer Developer { get; set; }
        public virtual Project Project { get; set; }

    }
}

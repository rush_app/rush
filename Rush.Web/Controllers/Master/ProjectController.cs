﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Rush.Model.Models;

namespace Rush.Web.Controllers.Master
{
    public class ProjectController : BaseApiController
    {
        // GET api/project
        public IEnumerable<Project> Get()
        {
            var projectList = DbContext.Projects.ToList();
            return projectList;
        }

        // GET api/project/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/project
        public void Post([FromBody]Project project)
        {
            DbContext.Entry(project).State = EntityState.Added;
            DbContext.SaveChanges();
        }

        // PUT api/project/5
        public void Put(int id, [FromBody]Project project)
        {
            DbContext.Entry(project).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        // DELETE api/project/5
        public void Delete(int id)
        {
            var projectToRemove = DbContext.Projects.Find(id);
            DbContext.Projects.Remove(projectToRemove);
            DbContext.SaveChanges();
        }
    }
}

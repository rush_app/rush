﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Rush.Model.Models;
using Rush.Web.Filters;

namespace Rush.Web.Controllers.Master
{
    public class DeveloperController : BaseApiController
    {
        // GET api/developer
        public IEnumerable<Developer> Get()
        {
            var developerList = DbContext.Developpers.ToList();
            return developerList;
        }

        // GET api/developer/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/developer
        [ValidateModel]
        public HttpResponseMessage Post([FromBody]Developer developer)
        {
            if (ModelState.IsValid)
            {
                DbContext.Entry(developer).State = EntityState.Added;
                DbContext.SaveChanges();
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }

        // PUT api/developer/5
        public void Put(int id, [FromBody]Developer developer)
        {

            DbContext.Entry(developer).State = EntityState.Modified;
            DbContext.SaveChanges();

        }

        // DELETE api/developer/5
        public void Delete(int id)
        {
            var developerToRemove = DbContext.Developpers.Find(id);
            DbContext.Developpers.Remove(developerToRemove);
            DbContext.SaveChanges();
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Rush.Model.Models;

namespace Rush.Web.ViewModels
{
    public class DeveloperAssignmentViewModel
    {
        public Developer Developer { get; set; }
        public IEnumerable<DeveloperAllocationViewModel> DeveloperAssignments { get; set; }
        public IEnumerable<IdAndText> WorkCategories { get; set; }
        public IEnumerable<Project> Projects { get; set; }
        public IEnumerable<BillingType> BillingTypes { get; set; }
    }

    public class IdAndText  
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }

    public class DeveloperAllocationViewModel
    {
        public int AllocationId { get; set; }
        [Required(ErrorMessage = "Developer Required")]
        public int DeveloperId { get; set; }
        [Required(ErrorMessage = "Project Required")]
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        [Required(ErrorMessage = "From Date Required")]
        public DateTime FromDate { get; set; }
        [Required(ErrorMessage = "To Date Required")]
        public DateTime ToDate { get; set; }
        public DateTime AllocatedDate { get; set; }
        [Required(ErrorMessage = "Work Category Required")]
        public int WorkingCategoryId { get; set; }
        public string WorkingCategory { get; set; }
        [Required(ErrorMessage = "Billing Type Required")]
        public int BillingTypeId { get; set; }
        public string BillingType { get; set; }

    }
}
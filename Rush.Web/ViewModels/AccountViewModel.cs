﻿
namespace Rush.Web.ViewModels
{
    public class AccountViewModel
    {
        public string UserId { get; set; }
        public string PassWord { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
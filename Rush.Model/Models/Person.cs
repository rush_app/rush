﻿
namespace Rush.Model.Models
{
    public class Person
    {
        public string Email { get; set; }
        public string Tel { get; set; }
        public bool IsActive { get; set; }
    }
}

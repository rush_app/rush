﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Rush.Model.Models;
using Rush.Web.Enums;
using Rush.Web.ViewModels;

namespace Rush.Web.Controllers.AssignDevelopers
{
    public class DeveloperAssignmentController : BaseApiController
    {
        // GET api/developerassignment
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/developerassignment/5
        public DeveloperAssignmentViewModel Get(int id)
        {
            var assignmentData = DbContext.ProjectAllocations.Where(c => c.DeveloperId == id).ToList();
            var projects = DbContext.Projects.ToList();
            var billingTypes = DbContext.BillingTypes.ToList();
            
            var workCategories = GetCategoryList();

            var assignmentList = assignmentData.Select(projectAllocation => new DeveloperAllocationViewModel
            {
                AllocationId = projectAllocation.AllocationId,
                DeveloperId = projectAllocation.DeveloperId,
                AllocatedDate = projectAllocation.AllocatedDate,
                ProjectId = projectAllocation.ProjectId,
                ProjectName = projectAllocation.Project.ProjectName,
                FromDate = projectAllocation.FromDate,
                ToDate = projectAllocation.ToDate,
                BillingType = projectAllocation.BillingType.Description,
                BillingTypeId = projectAllocation.BillingTypeId,
                WorkingCategory = workCategories.Single(c => c.Id == projectAllocation.WorkingCategoryId).Text,
                WorkingCategoryId = projectAllocation.WorkingCategoryId

            }).ToList();

            var developerAssignmentViewModel = new DeveloperAssignmentViewModel
            {
                Developer = DbContext.Developpers.Single(c => c.DeveloperId == id),
                DeveloperAssignments = assignmentList,
                BillingTypes = billingTypes,
                Projects = projects,
                WorkCategories = workCategories
            };

            return developerAssignmentViewModel;
        }

        // POST api/developerassignment
        public void Post([FromBody]DeveloperAllocationViewModel developerAllocation)
        {
            if (!ModelState.IsValid) return;
            var developerProjectAllocationToSave = SetObjectToSave(developerAllocation);
            DbContext.Entry(developerProjectAllocationToSave).State = EntityState.Added;
            DbContext.SaveChanges();
        }

        private static ProjectAllocation SetObjectToSave(DeveloperAllocationViewModel developerAllocation)
        {
            return new ProjectAllocation
            {
                AllocationId = developerAllocation.AllocationId,
                DeveloperId = developerAllocation.DeveloperId,
                ProjectId = developerAllocation.ProjectId,
                AllocatedDate = DateTime.Now,
                BillingTypeId = developerAllocation.BillingTypeId,
                WorkingCategoryId = developerAllocation.WorkingCategoryId,
                FromDate = developerAllocation.FromDate,
                ToDate = developerAllocation.ToDate,
                AllocatedBy = 1 //just for
            };
        }

        // PUT api/developerassignment/5
        public void Put(int id, [FromBody]DeveloperAllocationViewModel developerAllocation)
        {
            if (!ModelState.IsValid) return;
            var developerProjectAllocationToSave = SetObjectToSave(developerAllocation);
            DbContext.Entry(developerProjectAllocationToSave).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        // DELETE api/developerassignment/5
        public void Delete(int id)
        {
            var assignmentToRemove = DbContext.ProjectAllocations.Find(id);
            DbContext.ProjectAllocations.Remove(assignmentToRemove);
            DbContext.SaveChanges();
        }
    }
}

﻿
using System.Data.Entity.ModelConfiguration;

namespace Rush.Model.Models.Mapping
{
    public class ProjectMap : EntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            HasKey(t => t.ProjectId);

            Property(t => t.ProjectName).IsRequired().HasMaxLength(100);
            Property(t => t.IsActive).IsRequired();

            ToTable("Projects");
            Property(t => t.ProjectId).HasColumnName("ProjectId");
            Property(t => t.ProjectName).HasColumnName("ProjectName");
            Property(t => t.IsActive).HasColumnName("IsActive");
        }
    }
}

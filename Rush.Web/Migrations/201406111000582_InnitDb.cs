namespace Rush.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InnitDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Developers",
                c => new
                    {
                        DeveloperId = c.Int(nullable: false, identity: true),
                        DeveloperName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(maxLength: 30),
                        Tel = c.String(maxLength: 30),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DeveloperId);
            
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.Int(nullable: false, identity: true),
                        ProjectName = c.String(nullable: false, maxLength: 100),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ProjectId);
            
            CreateTable(
                "dbo.ProjectAllocations",
                c => new
                    {
                        AllocationId = c.Int(nullable: false, identity: true),
                        ProjectId = c.Int(nullable: false),
                        DeveloperId = c.Int(nullable: false),
                        AllocatedBy = c.Int(nullable: false),
                        AllocatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AllocationId)
                .ForeignKey("dbo.Developers", t => t.DeveloperId, cascadeDelete: true)
                .ForeignKey("dbo.Projects", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId)
                .Index(t => t.DeveloperId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectAllocations", "ProjectId", "dbo.Projects");
            DropForeignKey("dbo.ProjectAllocations", "DeveloperId", "dbo.Developers");
            DropIndex("dbo.ProjectAllocations", new[] { "DeveloperId" });
            DropIndex("dbo.ProjectAllocations", new[] { "ProjectId" });
            DropTable("dbo.ProjectAllocations");
            DropTable("dbo.Projects");
            DropTable("dbo.Developers");
        }
    }
}

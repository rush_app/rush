﻿
using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading;
using System.Web.Http;
using NUnit.Framework;
using Rush.Model.Models;
using Rush.Web.Controllers;
using Rush.Web.Controllers.Master;
using Assert = NUnit.Framework.Assert;

namespace Rush.Test
{
    [TestFixture]
    public class DeveloperControllerTest : BaseTestController
    {
        private readonly DeveloperController _controller = new DeveloperController();
        private const string ApiPath = "api/Developer/post?";

        [SetUp]
        public void Setup()
        {

        }

        [TestCase]
        public void ModelValidateForEmptyDeveloperName()
        {
            HttpClient client;
            HttpRequestMessage httpRequest;
            var developerTest = new Developer
            {
                Email = "test@gmail.com",
                IsActive = true,
                Tel = "01191212212"
            };
            SetRequetTestObject(developerTest, out client, out httpRequest, ApiPath);
            string errorMessage;
            using (HttpResponseMessage response = client.SendAsync(httpRequest, new CancellationTokenSource().Token).Result)
            {
                errorMessage = response.Content.ReadAsAsync<HttpError>().Result.Message;
            }
            Assert.AreEqual("Developer name required", errorMessage);
        }
    }
}

﻿
using System.ComponentModel;

namespace Rush.Web.Enums
{
    public enum WorkCategory
    {
        [Description("Full Time")]
        FullTime = 1,
        [Description("Part Time")]
        PartTime = 2,
        [Description("Time and Material")]
        TimeAndMaterial = 3
    }
}
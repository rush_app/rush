﻿
using System.ComponentModel.DataAnnotations;

namespace Rush.Model.Models
{
    public class BillingType
    {
        public int BillingTypeId { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public decimal BillablePercentage { get; set; }
    }
}

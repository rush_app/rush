﻿
using System.Data.Entity.ModelConfiguration;

namespace Rush.Model.Models.Mapping
{
    public class BillingTypeMap : EntityTypeConfiguration<BillingType>
    {
        public BillingTypeMap()
        {

            HasKey(t => t.BillingTypeId);

            Property(t => t.Description).IsRequired().HasMaxLength(50);
            Property(t => t.BillablePercentage).IsRequired();

            ToTable("BillingTypes");
            Property(t => t.BillingTypeId).HasColumnName("BillingTypeId");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.BillablePercentage).HasColumnName("BillablePercentage");

        }
    }
}

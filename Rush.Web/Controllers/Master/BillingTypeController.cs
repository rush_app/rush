﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Rush.Model.Models;

namespace Rush.Web.Controllers.Master
{
    public class BillingTypeController : BaseApiController
    {
        // GET api/billingtype
        public IEnumerable<BillingType> Get()
        {
            return DbContext.BillingTypes.ToList();
        }

       // POST api/billingtype
        public HttpResponseMessage Post([FromBody]BillingType billingType)
        {
            if (!ModelState.IsValid) return new HttpResponseMessage(HttpStatusCode.BadRequest);
            DbContext.Entry(billingType).State = EntityState.Added;
            DbContext.SaveChanges();
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        // PUT api/billingtype/5
        public void Put(int id, [FromBody]BillingType billingType)
        {
            if (!ModelState.IsValid) return;
            DbContext.Entry(billingType).State = EntityState.Modified;
            DbContext.SaveChanges();
        }

        // DELETE api/billingtype/5
        public void Delete(int id)
        {
            var billingTypeToRemove = DbContext.BillingTypes.Find(id);
            DbContext.BillingTypes.Remove(billingTypeToRemove);
            DbContext.SaveChanges();
        }
    }
}

﻿

var projApp = angular.module('rushApp');
var projectUrl = 'api/Project';

projApp.factory('projectRepository', function ($http) {
    return {
        getProjects: function (callback) {
            $http.get(projectUrl).success(callback);
        },
        insertProject: function (callback, project) {
            $http.post(projectUrl, project).success(callback);
        },
        updateProject: function (callback, project) {
            $http.put(projectUrl + '/' + project.ProjectId, project).success(callback);
        },
        deleteProject: function (callback, id) {
            $http.delete(projectUrl + '/' + id).success(callback);
        }
    };
});

//Controller
projApp.controller('projectController', function ($scope, projectRepository) {
    getProjects();

    function getProjects() {
        projectRepository.getProjects(function (results) {
            $scope.projectData = results;
        });
    }

    $scope.setScope = function (obj, action) {
        $scope.action = action;
        $scope.New = obj;
    };

    $scope.gridOptions = {
        data: 'projectData',
        showGroupPanel: true,
        columnDefs: [{ field: 'ProjectId', displayName: 'Project Id', width: '10%' },
            { field: 'ProjectName', displayName: 'Project Name', width: '30%' },
            { field: 'IsActive', displayName: 'Active', width: '10%' },
            {
                displayName: 'Options', cellTemplate: '<input type="button" ng-click="setScope(row.entity,\'edit\')" name="edit"  value="Edit">&nbsp;' +
                  '<input type="button" ng-click="delete(row.entity.ProjectId)"  name="delete"  value="Delete">&nbsp;' +
                  '<a href = "#/AssignDevelopers/{{row.entity.ProjectId}}" > Assign developers </a>', width: '25%'
            }
        ]
    };

    $scope.update = function () {
        if ($scope.action == 'edit') {
            projectRepository.updateProject(function () {
                var status = 'Project updated successfully';
                $scope.status = status;
                alert(status);
                getProjects();
            }, $scope.New);
        } else {
            projectRepository.insertProject(function () {
                alert('Project inserted successfully');
                getProjects();
            }, $scope.New);
        }
    };

    $scope.delete = function (id) {
        var retVal = confirm("Are you sure want to remove?");
        if (retVal == true) {
            projectRepository.deleteProject(function () {
                alert('Project deleted successfully');
                getProjects();
            }, id);
        }
    };

    $scope.reset = function () {
        $scope.New = angular.copy($scope.clearData);
    };

    $scope.customNavigate = function (msg) {
        alert(msg);
        $location.path("/AssignDevelopers/{{1}}");
    };
});
﻿var rushApp = angular.module('rushApp', ['ngRoute', 'ngGrid', 'ui.bootstrap']);

rushApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
            when('/Developers', {
                templateUrl: '/Views/Master/Developer.html',
                controller: 'developerController'
            }).
            when('/DeveloperAssignment/:id', {
                templateUrl: '/Views/AssignDevelopers/DeveloperAssignment.html',
                controller: 'developerAssignmentController'
            }).
            when('/Projects', {
                templateUrl: '/Views/Master/Project.html',
                controller: 'projectController'
            }).
            when('/BillingTypes', {
                templateUrl: '/Views/Master/BillingType.html',
                controller: 'billingTypeController'
            }).
            when('/ViewAllocation', {
                templateUrl: '/Views/Master/ProjectAllocation.html',
                controller: 'projectAllocationController'
            }).
            otherwise({
                redirectTo: '/AddNew'
            });
    }]);



﻿using System;
using System.Collections.Generic;
using Rush.Model.Models;

namespace Rush.Web.ViewModels
{
    public class ProjectAllocationViewModel
    {
        IEnumerable<ProjectDeveloperViewModel> AllocationList { get; set; }
    }

    public class ProjectDeveloperViewModel
    {
        public Project Project { get; set; }
        public IEnumerable<Developer> Developers { get; set; }
    }



    public class ProjectAllocationForm
    {
        public IEnumerable<AllocationDetail> AllocationDetails { get; set; }
    }

    public class AllocationDetail
    {
        public int DeveloperId { get; set; }
        public string DeveloperName { get; set; }
        public IEnumerable<DeveloperProjectDetail> DeveloperProjectDetails { get; set; }
    }

    public class DeveloperProjectDetail
    {
        public int DeveloperId { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int DateDiff { get; set; }
        public int StartDiff { get; set; }

        public string ToolTip { get; set; }
        public string WorkingCategory { get; set; }
        public string BillingType { get; set; }

    }

    public class ProjectSearch
    {
        public int ProjectId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

    }
}
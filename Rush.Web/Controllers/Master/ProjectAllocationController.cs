﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Rush.Model.Models;
using Rush.Web.ViewModels;

namespace Rush.Web.Controllers.Master
{
    public class ProjectAllocationController : BaseApiController
    {
        // GET api/projectallocat

        public ProjectAllocationForm Post(List<ProjectSearch> projects)
        {
            List<ProjectAllocation> developersAllocation;
            if (projects.Count > 0)
            {
                var serachList = projects.Select(c => c.ProjectId).ToList();

                developersAllocation =
                    DbContext.ProjectAllocations.Where(c => serachList.Contains(c.ProjectId)).ToList();
            }
            else
            {
                developersAllocation =
                    DbContext.ProjectAllocations.ToList();
            }
            // var ssDate = (from d in developersAllocation select d.FromDate);

            var minDate = new DateTime(2013, 1, 10);
            var groupDevList = developersAllocation.Select(c => c.Developer).Distinct().ToList();
            var workCategories = GetCategoryList();

            var allocationDetailList = new List<AllocationDetail>();
            foreach (var developer in groupDevList)
            {
                var allocationDetails = new AllocationDetail();
                var allocation = developersAllocation.Where(c => c.DeveloperId == developer.DeveloperId).ToList();
                var listAllocation = new List<DeveloperProjectDetail>();
                listAllocation.AddRange(allocation.Select(projectAllocation => new DeveloperProjectDetail
                {
                    DeveloperId = projectAllocation.DeveloperId,
                    ProjectId = projectAllocation.ProjectId,
                    ProjectName = projectAllocation.Project.ProjectName,
                    FromDate = projectAllocation.FromDate.ToShortDateString(),
                    ToDate = projectAllocation.ToDate,
                    DateDiff = Convert.ToInt32((projectAllocation.ToDate - projectAllocation.FromDate).TotalDays),
                    StartDiff = Convert.ToInt32((projectAllocation.FromDate - minDate).TotalDays),
                    BillingType = projectAllocation.BillingType.Description,
                    WorkingCategory = workCategories.Single(c => c.Id == projectAllocation.WorkingCategoryId).Text
                }));

                allocationDetails.DeveloperId = developer.DeveloperId;
                allocationDetails.DeveloperName = developer.DeveloperName;
                allocationDetails.DeveloperProjectDetails = listAllocation;
                allocationDetailList.Add(allocationDetails);
            }
            var ss = new List<ProjectSearch>();
            ss.Add(new ProjectSearch { ProjectId = 22 });
            return new ProjectAllocationForm
            {
                AllocationDetails = allocationDetailList,
            };
        }


    }
}

﻿
//var developerApp = angular.module('developerApp', ['ngGrid']);
//var rushApp = angular.module('rushApp');
var url = 'api/Developer';


rushApp.factory('developerRepository', function ($http) {
    return {
        getDevelopers: function (callback) {
            $http.get(url).success(callback);
        },
        insertDeveloper: function (callback, developer) {
            $http.post(url, developer).success(callback);
        },
        updateDeveloper: function (callback, developer) {
            $http.put(url + '/' + developer.DeveloperId, developer).success(callback);
        },
        deleteDeveloper: function (callback, id) {
            $http.delete(url + '/' + id).success(callback);
        }
    };
});

//Controller
rushApp.controller('developerController', function ($scope, developerRepository) {
    getDevelopers();

    function getDevelopers() {

        developerRepository.getDevelopers(function (results) {
            $scope.developerData = results;
        });
    }

    $scope.setScope = function (obj, action) {
        $scope.action = action;
        $scope.New = obj;
    };

    $scope.gridOptions = {
        data: 'developerData',
        showGroupPanel: true,
        columnDefs: [{ field: 'DeveloperId', displayName: 'Id', width: '5%' },
            { field: 'DeveloperName', displayName: 'Developer Name', width: '30%' },
            { field: 'IsActive', displayName: 'Active', width: '10%' },
            { field: 'Email', displayName: 'Email', width: '25%' },
            { field: 'Tel', displayName: 'Tel', width: '10%' },
            { displayName: 'Options', cellTemplate: '<input type="button" ng-click="setScope(row.entity,\'edit\')" name="edit"  value="Edit">' +
                '&nbsp;<input type="button" ng-click="delete(row.entity.DeveloperId)"  name="delete"  value="Delete">' +
                '&nbsp;<a href = "#/DeveloperAssignment/{{row.entity.DeveloperId}}" > Assignment </a>'
                , width: '45%'
            }
        ]
    };

    $scope.update = function () {
        if ($scope.action == 'edit') {
            developerRepository.updateDeveloper(function () {
                var status = 'Developer updated successfully';
                $scope.status = status;
                alert(status);
                getDevelopers();
            }, $scope.New);
        } else {
            developerRepository.insertDeveloper(function () {
                alert('Developer inserted successfully');
                getDevelopers();
            }, $scope.New);
        }
    };

    $scope.delete = function (id) {
        var retVal = confirm("Are you sure want to remove?");
        if (retVal == true) {
            developerRepository.deleteDeveloper(function () {
                alert('Developer deleted successfully');
                getDevelopers();
            }, id);
        }
    };
});
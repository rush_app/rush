﻿
rushApp.factory('projectAllocationRepository', function ($http) {

    var projectAllocationUrl = 'api/ProjectAllocation';
    var projectUrl = 'api/Project';


    return {
        getAllocation: function (callback, searchObj) {
            debugger;
            $http.post(projectAllocationUrl, searchObj).success(callback);
        },
        getProjects: function (callback) {
            $http.get(projectUrl).success(callback);
        }
    };
});

rushApp.controller('projectAllocationController', function ($scope, projectAllocationRepository) {
    $scope.projectIdList = [];
    $scope.Projects = [];

    getProjects();

    function getProjects() {
        projectAllocationRepository.getProjects(function (results) {
            $scope.projectData = results;
        });
    }

    getAllocation();

    function getAllocation() {

        projectAllocationRepository.getAllocation(function (results) {
            $scope.allocationData = results.AllocationDetails;
            drawAllocation(results.AllocationDetails, "#allocationDiv");

        }, $scope.projectIdList);
    }

    $scope.addProject = function (projectId) {
        var project = { ProjectId: projectId };
        debugger;
        //var index = $scope.projectIdList.indexOf(project);
        //if (index != -1) {
        //    $scope.projectIdList.splice(index, 1);
        //}
        var found = false;
        for (var count = 0; count < $scope.projectIdList.length; count++) {
            if ($scope.projectIdList[count].ProjectId == projectId) {
                $scope.projectIdList.splice(count, 1);
                found = true;
            }
        }
        if (found == false) {
            $scope.projectIdList.push(project);
        }
       getAllocation();
    };


    $scope.gridOptions = {
        data: 'projectData',
        columnDefs: [{ field: 'ProjectId', displayName: 'Project Id', visible: false, width: '10%' },
            { field: 'ProjectName', displayName: 'Project Name', width: '70%' },
            {
                displayName: '', cellTemplate: '<input type="button" ng-click="addProject(row.entity.ProjectId)" name="edit"  value="+">'
                 , width: '30%'
            }
        ],
        filterOptions: { filterText: '', useExternalFilter: false },
        showFilter: true
    };

    var getChildTags = function (allocationProjects) {
        var button = $("<input/>")
            .attr("type", "button")
            .attr("value", "?")
            .attr("devId", allocationProjects.DeveloperId)
            .attr("projId", allocationProjects.ProjectId)
            .attr("fromDate", allocationProjects.FromDate)
            .attr("toDate", allocationProjects.ToDate).addClass("viewDetailButton");

        var div = $("<div>").addClass("projectSlab");
        div.append($("<label>" + allocationProjects.ProjectName + ' : ' + "</label>").addClass("projectNameSlab"));
        div.append($("<input/>")
            .attr("style", "width: " + allocationProjects.StartDiff + "px")
            .addClass("startDateDiffSlab"));

        div.append($("<input/>")
            .attr("style", "width: " + allocationProjects.DateDiff + "px")
            .attr("value", allocationProjects.FromDate + ',' + allocationProjects.BillingType + ',' + allocationProjects.WorkingCategory + '')
            .addClass("dateDiffSlab"));
        div.append(button);
        return div;

    };

    var drawAllocation = function (data, parentTag) {
        $(parentTag).find("li").remove();
        for (var i = data.length - 1; i >= 0; i--) {
            var control = data[i];
            var listItem = $("<li>");
            listItem.attr("id", control.DeveloperId);
            var input = $("<label>" + control.DeveloperName + ' : ' + "</label>").addClass("deveNameSlab");
            listItem.append(input);
            var coverDiv = $("<div>");
            for (var j = control.DeveloperProjectDetails.length - 1; j >= 0; j--) {
                var tag = control.DeveloperProjectDetails[j];
                coverDiv.append(getChildTags(tag));
            }
            listItem.append(coverDiv);
            $(parentTag).find("ul").prepend(listItem);
        }


    };

});

